# Vending Machine Kata

An attempt at solving the Vending Machine [kata](https://github.com/guyroyse/vending-machine-kata)

## Get Started

### Running the tests

This project uses Facebook's [`Jest`](https://facebook.github.io/jest/) along with Jasmine to run tests. 
In the directory where you cloned `vending-machine-kata` simply

```
npm install
npm run buildwatch
```

This will start a runner watching your test files. [`red/green/refactor`](https://en.wikipedia.org/wiki/Test-driven_development) FTW!


### Linting

To ensure clean code, in another terminal `cd` into `vending-machine-kata`, and run the following every once in a while

```
eslint 'src/**/*.js'
```

### TODO 

- [ ] Provide some sort of user interface
