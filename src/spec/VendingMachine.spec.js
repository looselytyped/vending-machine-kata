import VendingMachine, {
  INSERT_COIN_MESSAGE,
  THANK_YOU_MESSAGE,
  ITEM_PRICE_MESSAGE,
  EXACT_CHANGE_ONLY_MESSAGE
} from '../VendingMachine';
import {
  COKE, CANDY, CHIPS
} from '../Item';
import Coin, {
  PENNY, NICKEL, DIME, QUARTER,
} from '../Coin';

let vm;

beforeEach(() => {
  vm = new VendingMachine();
});

describe('VendingMachine', () => {
  describe('construction', () => {
    it('should instantiate the vending machine', () => {
      expect(vm).toBeDefined();
    });

    it('should instantiate the vending machine with some inventory', () => {
      expect(vm.hasInventory()).toBe(true);
    });

    it('should display the right message', () => {
      expect(vm.display()).toBe(INSERT_COIN_MESSAGE);
    });

    it('should have no inserted coin inventory', () => {
      expect(vm.currentTotal()).toEqual(0);
    });
  });

  describe('inserting a coin', () => {
    it('should acknowledge the correct coin', () => {
      vm.insert(Coin.create(NICKEL));
      expect(vm.currentTotal()).toBe(5);
    });

    it('should ignore pennies', () => {
      vm.insert(Coin.create(PENNY));
      expect(vm.currentTotal()).toBe(0);
    });

    it('should cumulate the amount inserted', () => {
      vm.insert(Coin.create(NICKEL));
      vm.insert(Coin.create(QUARTER));
      expect(vm.currentTotal()).toBe(30);
    });

    it('should display the total amount inserted', () => {
      vm.insert(Coin.create(NICKEL));
      vm.insert(Coin.create(QUARTER));
      expect(vm.display()).toBe('0.30');
    });

    it('should correctly catalog the inserted coin inventory', () => {
      vm.insert(Coin.create(NICKEL));
      vm.insert(Coin.create(QUARTER));
      vm.insert(Coin.create(QUARTER));
      vm.insert(Coin.create(DIME));
      expect(vm.insertedCoinInventory[NICKEL]).toBe(1);
      expect(vm.insertedCoinInventory[QUARTER]).toBe(2);
      expect(vm.insertedCoinInventory[DIME]).toBe(1);
    });
  });

  describe('refunding', () => {
    it('should return the money', () => {
      vm.insert(Coin.create(NICKEL));
      vm.insert(Coin.create(NICKEL));
      vm.insert(Coin.create(DIME));
      vm.insert(Coin.create(QUARTER));

      vm.refund();
      expect(vm.currentTotal()).toBe(0);
    });

    it('should reset the display', () => {
      vm.insert(Coin.create(NICKEL));
      vm.insert(Coin.create(NICKEL));

      vm.refund();
      expect(vm.display()).toEqual(INSERT_COIN_MESSAGE);
    });
  });

  describe('inventory management', () => {
    it('should refund if there is no inventory', () => {
      // init a bare vending machine
      vm = new VendingMachine({});

      vm.insert(Coin.create(NICKEL));
      expect(vm.insertedCoinInventory[NICKEL]).toBe(0);
    });
  });

  describe('selecting products', () => {
    describe('with exact change', () => {
      beforeEach(() => {
        vm = new VendingMachine({ [COKE]: 3, [CANDY]: 3, [CHIPS]: 3 });
        // COKE is 1.00
        vm.insert(Coin.create(QUARTER));
        vm.insert(Coin.create(QUARTER));
        vm.insert(Coin.create(QUARTER));
        vm.insert(Coin.create(QUARTER));
        vm.dispense(COKE);
      });

      it('should update item inventory', () => {
        expect(vm.itemInventory[COKE]).toBe(2);
      });

      it('should update cash inventory', () => {
        expect(vm.coinInventory[QUARTER]).toBe(4);
        expect(vm.coinInventory[NICKEL]).toBe(0);
        expect(vm.coinInventory[DIME]).toBe(0);
      });

      it('should empty inserted coin inventory', () => {
        expect(vm.insertedCoinInventory[QUARTER]).toBe(0);
        expect(vm.insertedCoinInventory[NICKEL]).toBe(0);
        expect(vm.insertedCoinInventory[DIME]).toBe(0);
      });

      it('should thank the customer', () => {
        expect(vm.display()).toEqual(THANK_YOU_MESSAGE);
      });

      it('should thank the customer then reset display', () => {
        vm.display();
        expect(vm.display()).toEqual(INSERT_COIN_MESSAGE);
      });
    });

    describe('with less than item price', () => {
      beforeEach(() => {
        vm = new VendingMachine({ [COKE]: 3, [CANDY]: 3, [CHIPS]: 3 });
        // COKE is 1.00
        vm.insert(Coin.create(QUARTER));
        vm.insert(Coin.create(QUARTER));
        vm.insert(Coin.create(QUARTER));
        vm.dispense(COKE);
      });

      it('should NOT update item inventory', () => {
        expect(vm.itemInventory[COKE]).toBe(3);
      });

      it('should NOT update cash inventory', () => {
        expect(vm.coinInventory[QUARTER]).toBe(0);
        expect(vm.coinInventory[NICKEL]).toBe(0);
        expect(vm.coinInventory[DIME]).toBe(0);
      });

      it('should display the price of the selected item', () => {
        expect(vm.display()).toEqual(`${ITEM_PRICE_MESSAGE} 1.00`);
      });

      it('should display price, then reset display', () => {
        vm.display();
        expect(vm.display()).toEqual(INSERT_COIN_MESSAGE);
      });
    });
  });

  describe('calculating change', () => {
    describe('with exact amount', () => {
      beforeEach(() => {
        vm = new VendingMachine({ [COKE]: 3, [CANDY]: 3, [CHIPS]: 3 },
          { [NICKEL]: 3, [DIME]: 4, [QUARTER]: 5 });
        // CHIPS is 50
        vm.insert(Coin.create(QUARTER));
        vm.insert(Coin.create(QUARTER));
        vm.dispense(CHIPS);
      });

      it('should update the inventory', () => {
        expect(vm.itemInventory[CHIPS]).toBe(2);
      });

      it('should update cash inventory', () => {
        expect(vm.coinInventory[NICKEL]).toBe(3);
        expect(vm.coinInventory[DIME]).toBe(4);
        expect(vm.coinInventory[QUARTER]).toBe(7);
      });

      it('should display thank you', () => {
        expect(vm.display()).toBe(THANK_YOU_MESSAGE);
      });

      it('should reset display', () => {
        vm.display();
        expect(vm.display()).toBe(INSERT_COIN_MESSAGE);
      });
    });

    describe('with more than cost of item', () => {
      beforeEach(() => {
        vm = new VendingMachine({ [COKE]: 3, [CANDY]: 3, [CHIPS]: 3 },
          { [NICKEL]: 3, [DIME]: 4, [QUARTER]: 5 });
        // CHIPS is 50
        vm.insert(Coin.create(QUARTER));
        vm.insert(Coin.create(QUARTER));
        vm.insert(Coin.create(NICKEL));
        vm.dispense(CHIPS);
      });

      it('should update the inventory', () => {
        expect(vm.itemInventory[CHIPS]).toBe(2);
      });

      it('should update cash inventory', () => {
        expect(vm.coinInventory[NICKEL]).toBe(3);
        expect(vm.coinInventory[DIME]).toBe(4);
        expect(vm.coinInventory[QUARTER]).toBe(7);
      });

      it('should refund the change', () => {
        expect(vm.changeAmount).toBe(5);
      });

      it('should display thank you', () => {
        expect(vm.display()).toBe(THANK_YOU_MESSAGE);
      });

      it('should reset display', () => {
        vm.display();
        expect(vm.display()).toBe(INSERT_COIN_MESSAGE);
      });

      it('should reset change amount', () => {
        vm.display();
        vm.display();
        expect(vm.changeAmount).toBe(0);
      });
    });
  });


  describe('with no change possible', () => {
    beforeEach(() => {
      vm = new VendingMachine({ [COKE]: 3, [CANDY]: 3, [CHIPS]: 3 },
        { [NICKEL]: 0, [DIME]: 0, [QUARTER]: 0 });
      // CHIPS is 50
      vm.insert(Coin.create(QUARTER));
      vm.insert(Coin.create(QUARTER));
      vm.insert(Coin.create(NICKEL));
      vm.dispense(CHIPS);
    });

    it('should NOT update the inventory', () => {
      expect(vm.itemInventory[CHIPS]).toBe(3);
    });

    it('should NOT update cash inventory', () => {
      expect(vm.coinInventory[NICKEL]).toBe(0);
      expect(vm.coinInventory[DIME]).toBe(0);
      expect(vm.coinInventory[QUARTER]).toBe(0);
    });

    it('should ask for exact change', () => {
      expect(vm.display()).toBe(EXACT_CHANGE_ONLY_MESSAGE);
    });

    it('should reset display', () => {
      vm.display();
      expect(vm.display()).toBe(INSERT_COIN_MESSAGE);
    });
  });
});
