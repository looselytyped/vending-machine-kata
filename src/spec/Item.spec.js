import Item, {
  COKE, CANDY, CHIPS
} from '../Item';

describe('Item', () => {
  describe('construction', () => {
    it('should instantiate coke', () => {
      expect(Item.create(COKE)).toBeTruthy();
    });

    it('should instantiate candy', () => {
      expect(Item.create(CANDY)).toBeTruthy();
    });

    it('should instantiate chips', () => {
      expect(Item.create(CHIPS)).toBeTruthy();
    });

    it('should error on a non-existent type', () => {
      expect(() => {
        Item.create();
      }).toThrow();
    });
  });
});
