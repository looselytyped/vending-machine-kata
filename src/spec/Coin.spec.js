import Coin, {
  PENNY, NICKEL, DIME, QUARTER,
  PENNY_WEIGHT, NICKEL_WEIGHT, DIME_WEIGHT, QUARTER_WEIGHT
} from '../Coin';

describe('Coin', () => {
  describe('construction', () => {
    it('should instantiate a penny', () => {
      const p = Coin.create(PENNY);
      expect(p).toBeTruthy();
      expect(p.type).toEqual('Penny');
      expect(p.amount).toEqual(1);
      expect(p.weight).toEqual(PENNY_WEIGHT);
    });

    it('should instantiate a nicket', () => {
      const n = Coin.create(NICKEL);
      expect(n).toBeTruthy();
      expect(n.type).toEqual('Nickel');
      expect(n.amount).toEqual(5);
      expect(n.weight).toEqual(NICKEL_WEIGHT);

    });

    it('should instantiate a dime', () => {
      const d = Coin.create(DIME);
      expect(d).toBeTruthy();
      expect(d.type).toEqual('Dime');
      expect(d.amount).toEqual(10);
      expect(d.weight).toEqual(DIME_WEIGHT);
    });

    it('should instantiate a quarter', () => {
      const q = Coin.create(QUARTER);
      expect(q).toBeTruthy();
      expect(q.type).toEqual('Quarter');
      expect(q.amount).toEqual(25);
      expect(q.weight).toEqual(QUARTER_WEIGHT);
    });

    it('should default to a penny', () => {
      const p = Coin.create();
      expect(p).toBeTruthy();
      expect(p.amount).toEqual(1);
    });
  });
});
