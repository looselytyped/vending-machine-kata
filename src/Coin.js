export const PENNY = Symbol('Penny');
export const NICKEL = Symbol('Nickel');
export const DIME = Symbol('Dime');
export const QUARTER = Symbol('Quarter');

export const PENNY_WEIGHT = 2500;
export const NICKEL_WEIGHT = 5000;
export const DIME_WEIGHT = 2270;
export const QUARTER_WEIGHT = 5670;

export default class Coin {
  constructor(type, amount, weight) {
    this.type = type;
    this.amount = amount;
    this.weight = weight;
  }

  static create(type) {
    switch(type) {
      case PENNY:
        return new Coin('Penny', 1, 2500);
      case NICKEL:
        return new Coin('Nickel', 5, 5000);
      case DIME:
        return new Coin('Dime', 10, 2270);
      case QUARTER:
        return new Coin('Quarter', 25, 5670);
      default:
        return new Coin('Penny', 1, 2500);
    }
  }
}

