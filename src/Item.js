export default class Item {
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }

  static create(name) {
    switch(name) {
      case COKE:
        return new Item('Coke', 100);
      case CANDY:
        return new Item('Candy', 65);
      case CHIPS:
        return new Item('Chips', 50);
      default:
        throw new Error('Must be one of Coke, Candy, Chips');
    }
  }
}

export const COKE = Symbol('Coke');
export const CANDY = Symbol('Candy');
export const CHIPS = Symbol('Chips');
