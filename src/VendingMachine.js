import Item, {
  COKE, CANDY, CHIPS
} from './Item';
import Coin, {
  NICKEL, DIME, QUARTER,
  NICKEL_WEIGHT, DIME_WEIGHT, QUARTER_WEIGHT
} from './Coin';

export default class Vending {
  constructor(itemInventory = { [COKE]: 3, [CANDY]: 3, [CHIPS]: 3 },
    coinInventory = { [NICKEL]: 0, [DIME]: 0, [QUARTER]: 0 } ) {
    this.itemInventory = itemInventory;
    this.coinInventory = coinInventory;
    this.insertedCoinInventory = { [NICKEL]: 0, [DIME]: 0, [QUARTER]: 0 };
    this.displayMessage = null;
    this.changeAmount = 0;
  }

  hasInventory() {
    const hasInventory = Reflect.ownKeys(this.itemInventory).some((k) => this.itemInventory[k] > 0);
    return hasInventory ? true: false;
  }

  display() {
    if(this.displayMessage) {
      let ret;
      [ret, this.displayMessage, this.changeAmount] = [this.displayMessage, null, 0];
      return ret;
    }
    if(this.hasInventory) {
      return INSERT_COIN_MESSAGE;
    }
    if(!this.hasInventory) {
      return SOLD_OUT_MESSAGE;
    }
  }

  insert(coin) {
    switch(coin.weight) {
      case NICKEL_WEIGHT:
        this.insertedCoinInventory[NICKEL] = this.insertedCoinInventory[NICKEL] + 1;
        this.displayMessage = (this.currentTotal() / 100).toFixed(2);
        break;
      case DIME_WEIGHT:
        this.insertedCoinInventory[DIME] = this.insertedCoinInventory[DIME] + 1;
        this.displayMessage = (this.currentTotal() / 100).toFixed(2);
        break;
      case QUARTER_WEIGHT:
        this.insertedCoinInventory[QUARTER] = this.insertedCoinInventory[QUARTER] + 1;
        this.displayMessage = (this.currentTotal() / 100).toFixed(2);
        break;
      default:
        break;
    }
    if(!this.hasInventory()) {
      this.refund();
    }
  }

  currentTotal() {
    const total = Reflect.ownKeys(this.insertedCoinInventory).reduce((acc, k) => {
      if(this.insertedCoinInventory[k] > 0) {
        return acc + Coin.create(k).amount * this.insertedCoinInventory[k];
      }
      return acc;
    }, 0);
    return total;
  }

  emptyInsertedCoinInventory() {
    Reflect.ownKeys(this.insertedCoinInventory).forEach((k) => {
      this.insertedCoinInventory[k] = 0;
    });
  }

  refund() {
    this.emptyInsertedCoinInventory();
    this.displayMessage = INSERT_COIN_MESSAGE;
  }

  dispense(item) {
    const insertedAmt = this.currentTotal();
    const chosen = Item.create(item);
    if(insertedAmt < chosen.price) {
      this.displayMessage = `${ITEM_PRICE_MESSAGE} ${(chosen.price/100).toFixed(2)}`;
    } else {
      const [canMakeChange, change] = this.calculateChange(chosen.price, this.coinInventory, insertedAmt);
      if(canMakeChange) {
        this.itemInventory[item] = this.itemInventory[item] - 1;
        Reflect.ownKeys(this.insertedCoinInventory).forEach((k) => {
          // put the coins the user inserted in our inventory
          this.coinInventory[k] = this.coinInventory[k] + this.insertedCoinInventory[k];
          // while removing the ones we need for change
          this.coinInventory[k] = this.coinInventory[k] - change[k];
          if(change[k] > 0) {
            this.changeAmount = this.changeAmount + Coin.create(k).amount;
          }
        });
        this.displayMessage = THANK_YOU_MESSAGE;
        this.emptyInsertedCoinInventory();
      } else {
        this.displayMessage = EXACT_CHANGE_ONLY_MESSAGE;
      }
    }
  }

  calculateChange(cost, availableCoins, insertedAmt) {
    const changeNeeded = insertedAmt - cost;
    const change = { [NICKEL]: 0, [DIME]: 0, [QUARTER]: 0 };

    if(changeNeeded === 0) {
      return [true, change];
    }

    // we need to dispense change
    let remainder = changeNeeded;
    for(let key of [QUARTER, DIME, NICKEL]) {
      const coin = Coin.create(key);
      let availableNum = availableCoins[key];
      if(coin.amount > remainder) {
        continue;
      }
      while((remainder > 0) && (availableNum > 0)) {
        if(coin.amount <= remainder) {
          change[key] = change[key] + 1;
          availableNum = availableNum - 1;
          remainder = remainder - coin.amount;
        } else {
          break;
        }
      }
    }
    return [remainder === 0, change];
  }
}

export const INSERT_COIN_MESSAGE = 'INSERT COIN';
export const THANK_YOU_MESSAGE = 'THANK YOU';
export const ITEM_PRICE_MESSAGE = 'PRICE: ';
export const SOLD_OUT_MESSAGE = 'SOLD OUT';
export const EXACT_CHANGE_ONLY_MESSAGE = 'EXACT CHANGE ONLY';
